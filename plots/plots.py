# SPDX-FileCopyrightText: 2023 Lukas Fülle <https://codeberg.org/fulllness>
# SPDX-License-Identifier: MPL-2.0

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from sklearn.calibration import CalibrationDisplay

sns.set_theme(font="Fira Sans", rc={"axes.facecolor": "#f1eee5"})
sns.set_context("talk")

palette8 = ["#c00040", "#ff8000", "#ffff00", "#00c040", "#00c0ff", "#4000ff", "#800080", "#ff80c0"]
palette2 = ["#a32638", "#7d9aaa"]
dunkelgrau = "#575756"
palette2p1 = [*palette2, "#a9a28d"]

data_dir = "../data/"

envs = ["come", "pendxy"]
pretty_envs = {"come":"CopyMem", "pendxy":"Pendulum_xy"}


def ga_data(mode, env, policy, kernel_size):
	return pd.concat([
		pd.read_csv(
				F"{data_dir}{mode}_ga_{env}_{policy}{F'_k{kernel_size}' if kernel_size else ''}_{r}")
			.rename(columns={"best_eight":"eight_best"})
			.melt(id_vars=["episodes"], var_name="metric", value_name="return")
			.assign(
				returns={"pref":"learned", "only":"true"}[mode],
				task=pretty_envs[env],
				policy="memory" if policy=="history" else policy,
				kernel_size=kernel_size if kernel_size else "N/A",
				run=r)
		for r in range(1, 9)], ignore_index=True)

def rm_data(env, policy, kernel_size):
	return pd.concat([
		pd.read_csv(
				F"{data_dir}pref_ga_{env}_{policy}_k{kernel_size}_{r}_rm")
			.assign(proba=lambda x: 1 / (1+np.exp(-x.logits)))
			.melt(id_vars=["episodes"], var_name="metric", value_name="value")
			.assign(task=pretty_envs[env], kernel_size=kernel_size, run=r)
		for r in range(1, 9)], ignore_index=True)

def rm_point_data(env, kernel_size):
	l = []
	for r in range(1, 9):
		df = (pd.read_csv(F"{data_dir}pref_ga_{env}_history_k{kernel_size}_{r}_rm"))
		acc = df["true_acc"].mean()
		avg_loss = df["loss"].mean()
		df = df.tail(1)
		g = df["episodes"].max()
		df = (df.assign(
				preference_rate=df["prefs"]/g,
				training_rate=df["steps"]/g,
				accuracy=acc,
				error_rate=1-acc,
				loss=avg_loss)
			.melt(id_vars=["episodes"], var_name="metric", value_name="value")
			.assign(task=pretty_envs[env], kernel_size=kernel_size))
		l.append(df)
	return pd.concat(l, ignore_index=True)


def no_run(data):
	return data.drop(columns=["run"])

def downsample(data):
	resolution = 100
	dg = data["episodes"]
	data["episodes"] = (1 + (dg-1) * resolution // dg.max()) * dg.max() // 100
	return data


def ga_plot():
	sns.relplot(
		kind="line",
		errorbar=("pi", 50),
		data=downsample(no_run(pd.concat(
			[ga_data(mode, env, policy, kernel_size)
					.loc[lambda df: (df["metric"] == "eight_best")]
				for env in envs
				for (mode, policy, kernel_size) in [
					("pref", "history", 7),
					("pref", "history", 1),
					("only", "history", None),
					("only", "amnesia", None)]],
			ignore_index=True))),
		x="episodes",
		y="return",
		style="policy",
		hue="kernel_size",
		col="task",
		facet_kws={"sharex":False, "sharey":False},
		palette=palette2p1).fig.savefig(F"ga.pdf")

def rm_ecdf_plot():
		p = sns.displot(
			kind="ecdf",
			data=pd.concat(
				[rm_data(env, "history", kernel_size)
						.loc[lambda df: (df["metric"] == "proba")]
						.rename(columns={"value":"estimate"})
					for env in envs for kernel_size in [7, 1]],
				ignore_index=True),
			x="estimate",
			hue="kernel_size",
			row="task",
			hue_order=[7, 1],
			palette=palette2)
		#for a in p.axes_dict.values():
			#for lines, linestyle, legend_handle \
					#in zip(a.lines[::-1], ['-', '--'], a.get_legend_handles_labels()):
				#lines.set_linestyle(linestyle)
				#legend_handle[0].set_linestyle(linestyle)
		p.fig.savefig(F"rm_ecdf.pdf")

def rm_bar_plot():
	sns.set_context("talk", font_scale=0.9)
	sns.catplot(
		kind="bar",
		errorbar=("pi", 50),
		data=pd.concat(
			[rm_point_data(env, kernel_size)
					.loc[lambda df: ((df["metric"] == "loss")
						| (df["metric"] == "error_rate"))]
				for env in envs for kernel_size in [7, 1]],
			ignore_index=True),
		x="kernel_size",
		y="value",
		hue="kernel_size",
		row="metric",
		col="task",
		sharey="row",
		dodge=False,
		order=[7, 1],
		hue_order=[7, 1],
		row_order=["error_rate", "loss"],
		saturation=1,
		errcolor=dunkelgrau,
		edgecolor=dunkelgrau,
		palette=palette2).fig.savefig("rm_bar.pdf")

def cplot(data, **kws):
	prob = list(data["value"])
	istrue = [p>0.5 for p in prob]
	confidence = [p if p>0.5 else 1-p for p in prob]
	ax = plt.gca()
	CalibrationDisplay.from_predictions(
		istrue,
		confidence,
		strategy="quantile",
		n_bins=20,
		ax=ax,
		marker='.')

def rm_calib_plot():
	sns.set_context("paper")
	g = sns.FacetGrid(
		pd.concat(
			[rm_data(env, "history", kernel_size)
				.loc[lambda df: df["metric"] == "proba"]
			 for env in envs for kernel_size in [7, 1]]),
		col="task",
		hue="kernel_size",
		xlim=(0.5, 1),
		ylim=(0.5, 1),
		palette=palette2)
	g.map_dataframe(cplot)
	plt.savefig("rm_calib.pdf")


if __name__ == "__main__":
	ga_plot()
	rm_calib_plot()
	rm_bar_plot()
	rm_ecdf_plot()
