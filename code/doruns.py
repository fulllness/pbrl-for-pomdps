# SPDX-FileCopyrightText: 2023 Lukas Fülle <https://codeberg.org/fulllness>
# SPDX-License-Identifier: MPL-2.0


from csv import writer as make_csv_writer
from random import choice as random_choice, randrange
from time import perf_counter

import torch
from torch.utils.tensorboard import SummaryWriter

from concretepolicies import AmnesiaPolicy
from episodes import corrupted_return, observations_actions_and_return, true_return
from evolutionalgos import EvaluatedPolicy, genetic_algorithm_step, openai_es_step
from pomdp import make_pomdp
from returnlearning import ConvergenceDetector, MovingAverage, PreferenceBuffer, corrupted_pref, test_and_train
from returnmodel import ReturnModel

#from torch.multiprocessing import set_start_method
#set_start_method("spawn")

use_rm = False
writer = None
rm_csv_fileobject = None


def setup_rm(env_str, acc_patience, loss_patience, kernel_size_, neurons_per_layer, pref_corruption=0, max_rec_len_=None, make_writer=False, rm_csv_filename=None): # make writer for testing rm without ea
	global use_rm, rm_env, kernel_size, rm_neurons_per_layer, pref_std, rm, pb, adam, total_prefs, train_steps, curr_acc, true_acc, acc_cd, loss_cd, pref_crp, max_rec_len, writer, rm_csv_fileobject, rm_csv_writer
	use_rm = True
	rm_env = make_pomdp(env_str)
	kernel_size = kernel_size_
	rm_neurons_per_layer = neurons_per_layer
	rm = ReturnModel.for_env(env_str, kernel_size, rm_neurons_per_layer)
	rm.cuda()
	pb = PreferenceBuffer(3000)
	adam = torch.optim.Adam(rm.parameters(), 0.002)
	total_prefs = 0
	train_steps = 0
	curr_acc = MovingAverage(0.9, 0)
	true_acc = MovingAverage(0.9, 0.5)
	acc_cd = ConvergenceDetector(acc_patience, 0.05)
	loss_cd = ConvergenceDetector(loss_patience, 0.01)
	pref_crp = pref_corruption
	max_rec_len = max_rec_len_
	if make_writer:
		writer = SummaryWriter(comment=F"_testrm,loss_p={loss_patience}_{env_str},reclen={max_rec_len}")
	if rm_csv_filename is None:
		rm_csv_writer = None
	else:
		rm_csv_fileobject = open(rm_csv_filename, "x", newline="")
		rm_csv_writer = make_csv_writer(rm_csv_fileobject)
		rm_csv_writer.writerow(["episodes", "pref_acc", "true_acc", "logits", "loss", "prefs", "steps"])

def test_rm(mutant_generator, generations):
	global e
	for e in range(1, generations+1):
		test_train_rm(mutant_generator, nocopy=True)
	close_fileobject(rm_csv_fileobject)

def test_train_rm(mutant_generator, nocopy=False): # no copy for testing rm without ea
	global total_prefs, train_steps
	new_prefs, new_train_steps, logits, loss = test_and_train(
		rm,
		mutant_generator,
		rm_env,
		max_rec_len,
		corrupted_pref(pref_crp),
		pb,
		"cuda",
		32,
		adam,
		loss_cd,
		curr_acc,
		acc_cd,
		true_acc)
	total_prefs += new_prefs
	train_steps += new_train_steps

	# Logging
	if writer is not None:
		writer.add_scalar("RM/Preference-based accuracy", curr_acc.get(), e)
		writer.add_scalar("RM/True accuracy", true_acc.get(), e)
		writer.add_scalar("RM/Last logits", logits, e)
		writer.add_scalar("RM/Last loss", loss, e)
		writer.add_scalar("RM/Total preferences", total_prefs, e)
		writer.add_scalar("RM/Total training steps", train_steps, e)
	if rm_csv_writer is not None:
		rm_csv_writer.writerow([e, curr_acc.get(), true_acc.get(), logits, loss, total_prefs, train_steps])

	if not nocopy:
		rmc = ReturnModel.for_env(env_str, kernel_size, rm_neurons_per_layer)
		rmc.load_state_dict(rm.state_dict())
		rmc.cpu()
		rmc.share_memory()
		return rmc

def setup_oe(policy_class, env_str_, lr_, count_, std_, n_p_, return_corruption=0, log_tensorboard_=True, csv_filename=None):
	global po, e, writer, test_env, render_env, env_str, count, std, lr, n_p, ret_crp, log_tensorboard, csv_fileobject, csv_writer
	po = policy_class(env_str_)
	#po = po.new_from_params(torch.zeros_like(po.parameter_vector()))  # Comment in to initialize policy with zeros
	e = 0
	env_str = env_str_
	count = count_
	std = std_
	lr = lr_
	n_p = n_p_
	ret_crp = return_corruption
	log_tensorboard = log_tensorboard_
	test_env = make_pomdp(env_str)
	render_env = make_pomdp(env_str, True)
	if log_tensorboard:
		writer = SummaryWriter(comment=F"_oe,α={lr},λ={count},σ={std},c_r={ret_crp}_{env_str}_{policy_class}")
	if csv_filename is None:
		csv_writer = None
	else:
		csv_fileobject = open(csv_filename, "x", newline="")
		csv_writer = make_csv_writer(csv_fileobject)
		csv_writer.writerow(["episodes", "best_one", "best_eight", "eight_new", "central"])

def setup_ga(policy_class, env_str_, p_count, m_count_, std_, n_p_, return_corruption=0, log_tensorboard_=True, logc="", csv_filename=None):
	global pas, e, writer, test_env, render_env, env_str, m_count, std, lr, n_p, ret_crp, log_tensorboard, csv_fileobject, csv_writer
	pas = [EvaluatedPolicy(policy_class(env_str_), 0.0) for _ in range(p_count)]
	e = 0
	env_str = env_str_
	m_count = m_count_
	std = std_
	n_p = n_p_
	ret_crp = return_corruption
	log_tensorboard = log_tensorboard_
	test_env = make_pomdp(env_str)
	render_env = make_pomdp(env_str, True)
	if log_tensorboard:
		writer = SummaryWriter(comment=F"_ga,μ={p_count},λ={m_count},σ={std},c_r={ret_crp}_{env_str}_{policy_class}{F'_rm{kernel_size}' if use_rm else ''}_{logc}")
	if csv_filename is None:
		csv_writer = None
	else:
		csv_fileobject = open(csv_filename, "x", newline="")
		csv_writer = make_csv_writer(csv_fileobject)
		csv_writer.writerow(["episodes", "best_one", "best_eight", "eight_new"])

def train_oe(episodes):
	global po, e
	while e <= episodes:
		if use_rm:
			rmc = test_train_rm(po.mutant_generator(std))
		po, sortedEvaledPos, mr = openai_es_step(
			po,
			env_str,
			rmc.predict_po_env if use_rm else corrupted_return(ret_crp),
			count,
			std,
			lr,
			n_p)

		# Logging
		bestOne = true_return(
			sortedEvaledPos[-1].policy,
			render_env if e%1300<=count else test_env)
		bestEight = sum(
				true_return(ep.policy, test_env) 
				for ep in sortedEvaledPos[-8:]
			) / 8
		eightNew = sum(
				true_return(po.make_mutant(std), test_env) for _ in range(8)
			) / 8
		central = true_return(po, test_env)
		if log_tensorboard:
			writer.add_scalar("OE/Best mutant reevaluated", bestOne, e)
			writer.add_scalar("OE/Best 8 reevaluated", bestEight, e)
			writer.add_scalar("OE/8 new mutants", eightNew, e)
			writer.add_scalar("OE/Central policy", central, e)
			writer.add_scalar("OE/Mean better-of-pair predicted return", mr, e)
		if csv_writer is not None:
			csv_writer.writerow([e, bestOne, bestEight, eightNew, central])

		e+=count
		del sortedEvaledPos # Helps with file descriptor leak mentioned in evolutionalgos.py
	close_fileobject(csv_fileobject)
	close_fileobject(rm_csv_fileobject)

def train_ga(episodes):
	global pas, e
	from heapq import nlargest
	while e <= episodes:
		if use_rm:
			rmc = test_train_rm(ga_mutant_generator(pas))
		pas = genetic_algorithm_step(
			pas,
			env_str,
			rmc.predict_po_env if use_rm else corrupted_return(ret_crp),
			m_count,
			std,
			n_p)

		# Logging
		bestOne = true_return(
			max(pas).policy, 
			render_env if e%1300<=m_count+len(pas) else test_env)
		bestEight = sum(
				true_return(ea.policy, test_env) for ea in nlargest(8, pas)
			) / 8
		mutGen = ga_mutant_generator(pas)
		eightNew = sum(true_return(next(mutGen), test_env) for _ in range(8))/8
		if log_tensorboard:
			writer.add_scalar("GA/Best mutant reevaluated", bestOne, e)
			writer.add_scalar("GA/Best 8 reevaluated", bestEight, e)
			writer.add_scalar("GA/8 new mutants", eightNew, e)
		if csv_writer is not None:
			csv_writer.writerow([e, bestOne, bestEight, eightNew])

		e += m_count + len(pas)
	close_fileobject(csv_fileobject)
	close_fileobject(rm_csv_fileobject)

def ga_mutant_generator(pas):
	"""Yields policies from the same distribution like the ones generated in training.
	"""
	while True:
		p = random_choice(pas).policy
		yield p.make_mutant(std) if m_count > randrange(m_count+len(pas)) else p

def close_fileobject(fileobject):
	if fileobject is not None:
		fileobject.close()


# Old debugging tests. Probably don't work anymore.

def time_rm(n, device):
	e = make_pomdp("Pendulum-v1")
	p = AmnesiaPolicy("Pendulum-v1")
	oa,r = observations_actions_and_return(p,e);
	oa = oa.to(device)
	rm = ReturnModel.for_env("Pendulum-v1", 7, 12).to(device)
	oas = [i*oa for i in range(n)]
	st = [torch.stack([i*oa]) for i in range(n)]
	t0 = perf_counter()
	rm(torch.stack(oas))
	t1 = perf_counter()
	for i in range(n):
		rm(st[i])
	t2 = perf_counter()
	print(F"p{t1-t0} s{t2-t1}")
	# Approx. times in s for n=100:
	#           cpu     cuda
	# parallel  0.02    0.003
	# serial    0.20    0.23
