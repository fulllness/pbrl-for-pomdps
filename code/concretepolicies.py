# SPDX-FileCopyrightText: 2023 Lukas Fülle <https://codeberg.org/fulllness>
# SPDX-License-Identifier: MPL-2.0


from collections import deque

from torch import Tensor, cat as cat_tensors, nn, squeeze, unsqueeze, zeros as zero_tensor, tensor

from abstractpolicy import AbstractPolicy


class TigerAvoider(AbstractPolicy):
	"""Optimal policy that has no memory
	"""

	def __init__(self,env_str):
		super(TigerAvoider, self).__init__(env_str)
		self.linear = nn.Linear(2, 3)

	def decide(self, obs):
		if obs==0:
			return 2
		elif obs==-1:
			return 1
		elif obs==1:
			return 0
		else:
			raise EnvironmentError

	def reset(self):
		pass


class AmnesiaPolicy(AbstractPolicy):
	"""Policy without memory
	"""

	def __init__(self, env_str: str) -> None:
		super(AmnesiaPolicy, self).__init__(env_str)
		if env_str == "CopyMem":
			self.linear_relu_stack = nn.Sequential(
				nn.Linear(1, 16),
				nn.ReLU(),
				nn.Linear(16, 16),
				nn.ReLU(),
				nn.Linear(16, 1),
			)
			self.decide = self.raw
		elif env_str == "TigerLair":
			self.linear_relu_stack = nn.Sequential(
				nn.Linear(1, 16),
				nn.ReLU(),
				nn.Linear(16, 16),
				nn.ReLU(),
				nn.Linear(16, 3),
			)
			self.decide = self.n_ary_det
		elif env_str == "CartPole-v1":
			# Architectures from https://github.com/openai/gym/wiki/Leaderboard (numbers of hidden neurons per layer)
			# 24
			# 128
			# 16 16
			# 24 24
			# 32 24
			# 32 64
			# 64 64
			# 64 64
			# 128 128
			# 256 256
			# 128 64 64
			# 64 32 16 4
			self.linear_relu_stack = nn.Sequential(
				nn.Linear(4, 64),
				nn.ReLU(),
				nn.Linear(64, 64),
				nn.ReLU(),
				nn.Linear(64, 1),
			)
			self.decide = self.binary
		elif env_str == "Pendulum-v1":
			# 3 3
			# 32 64
			# 400 300
			# 400 300
			# 8 8 8
			self.linear_relu_stack = nn.Sequential(
				nn.Linear(3, 32),
				nn.ReLU(),
				nn.Linear(32, 64),
				nn.ReLU(),
				nn.Linear(64, 1),
			)
			self.decide = self.raw
		elif env_str == "Pendulum-v1-only_v":
			self.linear_relu_stack = nn.Sequential(
				nn.Linear(1, 32),
				nn.ReLU(),
				nn.Linear(32, 64),
				nn.ReLU(),
				nn.Linear(64, 1),
			)
			self.decide = self.raw
		elif env_str == "Pendulum-v1-only_xy":
			self.linear_relu_stack = nn.Sequential(
				nn.Linear(2, 32),
				nn.ReLU(),
				nn.Linear(32, 64),
				nn.ReLU(),
				nn.Linear(64, 1),
			)
			self.decide = self.raw
		elif env_str == "LunarLander-v2":
			# 128
			# 256
			# 8 8
			# 64 32
			# 64 64
			# 64 64
			# 128 64
			# 128 64
			# 128 128
			# 128 128
			# 128 128
			# 150 120
			self.linear_relu_stack = nn.Sequential(
				nn.Linear(8, 128),
				nn.ReLU(),
				nn.Linear(128, 64),
				nn.ReLU(),
				nn.Linear(64, 4),
			)
			self.decide = self.n_ary_sto
		elif env_str == "LunarLander-v2-only_xya":
			self.linear_relu_stack = nn.Sequential(
				nn.Linear(3, 128),
				nn.ReLU(),
				nn.Linear(128, 64),
				nn.ReLU(),
				nn.Linear(64, 4),
			)
			self.decide = self.n_ary_sto
		elif env_str == "BipedalWalker-v3":
			# Numbers of hidden layers from https://github.com/NickKaparinos/OpenAI-Gym-Projects/blob/master/Box2D/BipedalWalker/main.py and https://github.com/shnippi/GYM/blob/master/TD3/td3_main.py
			h1 = (256+400)//2
			h2 = (256+300)//2
			self.linear_relu_stack = nn.Sequential(
				nn.Linear(24, h1),
				nn.ReLU(),
				nn.Linear(h1, h2),
				nn.ReLU(),
				nn.Linear(h2, 4),
			)
			self.decide = self.raw
		else:
			raise ValueError(F"Unknown environment for AmnesiaPolicy: {env_str}")
		self.requires_grad_(False)
		
	def reset(self) -> None:
		pass

	def forward(self, x: Tensor) -> Tensor:
		return self.linear_relu_stack(x)


class HistoryPolicy(AbstractPolicy):
	"""Policy that chooses actions based on a fixed-length history
	"""

	def __init__(self, env_str: str) -> None:
		super(HistoryPolicy, self).__init__(env_str)
		if env_str == "CopyMem":
			self.history_len = 4
			self.observation_dim = 1
			self.linear_relu_stack = nn.Sequential(
				nn.Linear(self.history_len, 16),
				nn.ReLU(),
				nn.Linear(16, 16),
				nn.ReLU(),
				nn.Linear(16, 1)
			)
			self.decide = self.raw
		elif env_str == "TigerLair":
			self.history_len = 8
			self.observation_dim = 1
			self.linear_relu_stack = nn.Sequential(
				nn.Linear(self.history_len, 16),
				nn.ReLU(),
				nn.Linear(16, 16),
				nn.ReLU(),
				nn.Linear(16, 3)
			)
			self.decide = self.n_ary_sto
		elif env_str == "Pendulum-v1-only_v":
			self.history_len = 8
			self.observation_dim = 1
			self.linear_relu_stack = nn.Sequential(
				nn.Linear(self.history_len, 32),
				nn.ReLU(),
				nn.Linear(32, 64),
				nn.ReLU(),
				nn.Linear(64, 1)
			)
			self.decide = self.raw
		elif env_str == "Pendulum-v1-only_xy":
			self.history_len = 8
			self.observation_dim = 2
			self.linear_relu_stack = nn.Sequential(
				nn.Linear(self.observation_dim * self.history_len, 32),
				nn.ReLU(),
				nn.Linear(32, 64),
				nn.ReLU(),
				nn.Linear(64, 1)
			)
			self.decide = self.raw
		elif env_str == "LunarLander-v2-only_xya":
			self.history_len = 6
			self.observation_dim = 3
			l1 = 256
			l2 = 128
			self.linear_relu_stack = nn.Sequential(
				nn.Linear(self.observation_dim * self.history_len, l1),
				nn.ReLU(),
				nn.Linear(l1, l2),
				nn.ReLU(),
				nn.Linear(l2, 4)
			)
			self.decide = self.n_ary_sto
		else:
			raise ValueError(
				F"Unknown environment for HistoryPolicy: {env_str}")
		self.reset()
		self.requires_grad_(False)

	def reset(self) -> None:
		self.history = deque(
			self.history_len * [tensor(self.observation_dim*[0.0])],
			maxlen=self.history_len)

	def forward(self, x: Tensor) -> Tensor:
		self.history.appendleft(x)
		return self.linear_relu_stack(cat_tensors(list(self.history)))


class MemoryPolicy(AbstractPolicy):
	"""Policy with recurrent memory: GRUs intertwined with feed-forward layers (similar to skip connections).
	"""

	def __init__(self, env_str: str) -> None:
		super(MemoryPolicy, self).__init__(env_str)
		if env_str == "CopyMem":
			l1 = 16
			l2 = 16
			g = 8
			self.gru_stack = nn.ModuleList([
				nn.GRUCell(1, g),
				nn.GRUCell(g+l1, g),
			])
			self.linear_relu_list = nn.ModuleList([
				nn.Sequential(
					nn.Linear(1, l1),
					nn.ReLU(),
				),
				nn.Sequential(
					nn.Linear(g+l1, l2),
					nn.ReLU(),
				),
			])
			self.linear = nn.Linear(g+l2, 1)
			self.decide = self.raw
		elif env_str == "TigerLair":
			l1 = 16
			l2 = 16
			g = 8
			self.gru_stack = nn.ModuleList([
				nn.GRUCell(1, g),
				nn.GRUCell(g+l1, g),
			])
			self.linear_relu_list = nn.ModuleList([
				nn.Sequential(
					nn.Linear(1, l1),
					nn.ReLU(),
				),
				nn.Sequential(
					nn.Linear(g+l1, l2),
					nn.ReLU(),
				),
			])
			self.linear = nn.Linear(g+l2, 3)
			self.decide = self.n_ary_det
		elif env_str == "Pendulum-v1":
			self.gru_stack = nn.ModuleList([
				nn.GRUCell(3, 16),
				nn.GRUCell(16+32, 16),
			])
			self.linear_relu_list = nn.ModuleList([
				nn.Sequential(
					nn.Linear(3, 32),
					nn.ReLU(),
				),
				nn.Sequential(
					nn.Linear(16+32, 64),
					nn.ReLU(),
				),
			])
			self.linear = nn.Linear(16+64, 1)
			self.decide = self.raw
		elif env_str == "Pendulum-v1-only_v":
			l1 = 32
			l2 = 64
			g = 16
			self.gru_stack = nn.ModuleList([
				nn.GRUCell(1, g),
				nn.GRUCell(g+l1, g),
			])
			self.linear_relu_list = nn.ModuleList([
				nn.Sequential(
					nn.Linear(1, l1),
					nn.ReLU(),
				),
				nn.Sequential(
					nn.Linear(g+l1, l2),
					nn.ReLU(),
				),
			])
			self.linear = nn.Linear(g+l2, 1)
			self.decide = self.raw
		elif env_str == "Pendulum-v1-only_xy":
			l1 = 32
			l2 = 64
			g = 16
			self.gru_stack = nn.ModuleList([
				nn.GRUCell(2, g),
				nn.GRUCell(g+l1, g),
			])
			self.linear_relu_list = nn.ModuleList([
				nn.Sequential(
					nn.Linear(2, l1),
					nn.ReLU(),
				),
				nn.Sequential(
					nn.Linear(g+l1, l2),
					nn.ReLU(),
				),
			])
			self.linear = nn.Linear(g+l2, 1)
			self.decide = self.raw
		elif env_str == "LunarLander-v2-only_xya":
			l1 = 128
			l2 = 64
			g = 32
			self.gru_stack = nn.ModuleList([
				nn.GRUCell(3, g),
				nn.GRUCell(g+l1, g),
			])
			self.linear_relu_list = nn.ModuleList([
				nn.Sequential(
					nn.Linear(3, l1),
					nn.ReLU(),
				),
				nn.Sequential(
					nn.Linear(g+l1, l2),
					nn.ReLU(),
				),
			])
			self.linear = nn.Linear(g+l2, 4)
			self.decide = self.n_ary_sto
		else:
			raise ValueError(F"Unknown environment for MemoryPolicy: {env_str}")
		self.reset()
		self.requires_grad_(False)

	def reset(self) -> None:
		self.memory = [
			zero_tensor(1, gru.hidden_size) for gru in self.gru_stack] # 1: Dimension of batch

	def forward(self, x: Tensor) -> Tensor:
		iput = unsqueeze(x, 0) # Add batch dimension
		for (l, gru) in enumerate(self.gru_stack):
			gruOut = gru(iput, self.memory[l])
			self.memory[l] = gruOut
			iput = cat_tensors((gruOut, self.linear_relu_list[l](iput)), 1)
		return squeeze(self.linear(iput), 1) # Remove batch dimension
