# SPDX-FileCopyrightText: 2023 Lukas Fülle <https://codeberg.org/fulllness>
# SPDX-License-Identifier: MPL-2.0


from random import getrandbits, random as random_std_uniform
from time import sleep
from typing import Tuple, Union

from gym import make as make_env # type: ignore
from numpy import array, float32, ndarray


Environment = Union["CopyMem", "TigerLair", "GymWrapper"]


def make_pomdp(env_str: str, render=False) -> Environment:
	return (CopyMem() if env_str=="CopyMem"
		else TigerLair() if env_str=="TigerLair"
		else GymWrapper(env_str, render))


def cast_observation(obs: float) -> ndarray:
	return array([obs], dtype=float32)


class CopyMem():
	def __init__(self) -> None:
		self.memlen = 4

	def reset(self) -> ndarray:
		self.steps = 0
		self.mem = [random_std_uniform() for _ in range(self.memlen)]
		return cast_observation(self.mem[0])

	def step(self, action: Union[float, ndarray]) -> Tuple[ndarray, float, bool]:
		self.steps += 1
		if self.steps < self.memlen:
			return (cast_observation(self.mem[self.steps]), -abs(action), False)
		elif self.steps < 2*self.memlen:
			return (
				cast_observation(0),
				-abs(action - self.mem[self.steps - self.memlen]),
				self.steps == 2*self.memlen-1
				)
		else:
			raise OverflowError("The episode has finished")

	def close(self) -> None:
		pass


class TigerLair():
	def __init__(self) -> None:
		pass

	def reset(self) -> ndarray:
		self.steps = 0
		self.tiger_pos = getrandbits(1)
		self.step = self.step_impl
		return cast_observation(0) # Initial observation

	def step_impl(self, action: Union[int, ndarray]) -> \
			Tuple[ndarray, float, bool]:
		if type(action) != int:
			raise TypeError
		self.steps += 1
		if action == 2: # Listen
			return (
				cast_observation(self.tiger_pos * 2 - 1 # tiger_pos is 0 or 1, but observation is -1 or 1
					if random_std_uniform() < 0.85
					else 1 - 2 * self.tiger_pos),
				-1.0, self.steps>=100) # Truncate episode
		elif action == 0 or action == 1: # Open a door
			self.step = lambda _: (_ for _ in ()).throw(OverflowError( # Since lambdas are not allowed to contain statements ...
				"A door was already opened, so the episode has finished"))
			return (
				cast_observation(0),
				-100.0 if action == self.tiger_pos else 10.0,
				True)
		else:
			raise ValueError(F"Illegal action in Tiger Lair: {action}")

	def close(self) -> None:
		pass


class GymWrapper():
	def __init__(self, env_str: str, render: bool) -> None:
		pend = "Pendulum-v1"
		lula = "LunarLander-v2"
		(self.censor_observation, base_env) = {
				pend : (self.complete_observation, pend),
				pend+"-only_v" : (self.pendulum_velocity_only, pend),
				pend+"-only_xy" : (self.pendulum_position_only, pend),
				lula : (self.complete_observation, lula),
				lula+"-only_xya" : (self.lunar_lander_position_angle_only, lula)
			}[env_str]
		self.env = make_env(base_env, render_mode="human" if render else None);
		self.render = render


	# Different functions for censoring observations, one of which is assigned to self.censor_observation

	def complete_observation(self, obs: ndarray) -> ndarray:
		return obs

	def pendulum_velocity_only(self, obs: ndarray) -> ndarray:
		return obs[2:3]

	def pendulum_position_only(self, obs: ndarray) -> ndarray:
		return obs[0:2]

	def lunar_lander_position_angle_only(self, obs: ndarray) -> ndarray:
		return obs[[0, 1, 4]]


	def reset(self) -> ndarray:
		return self.censor_observation(self.env.reset()[0])

	def step(self, action: Union[int, ndarray]) -> Tuple[ndarray, float, bool]:
		if self.render:
			sleep(1/30)
		observation, reward, terminated, truncated, info = self.env.step(action)
		return (
			self.censor_observation(observation),
			reward,
			terminated or truncated)

	def close(self) -> None:
		self.env.close()
