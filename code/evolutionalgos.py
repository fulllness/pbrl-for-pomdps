# SPDX-FileCopyrightText: 2023 Lukas Fülle <https://codeberg.org/fulllness>
# SPDX-License-Identifier: MPL-2.0


from dataclasses import dataclass, field
from heapq import heappush, heappushpop
from multiprocessing.sharedctypes import Synchronized as SharedValueT # type: ignore
# ↑ type of torch.multiprocessing.Value. "ignore" to supress mypy error.
from multiprocessing.synchronize import Lock as LockT # type of multiprocessing.Lock
from random import choice as random_choice
from typing import Callable, Generator, Iterator, List, Tuple

from sortedcontainers import SortedList # type: ignore
from torch import set_num_threads, zeros_like
from torch.multiprocessing import \
	JoinableQueue, Lock, Process, SimpleQueue, Value as SharedValue, cpu_count # same speed as standard multiprocessing module

from abstractpolicy import AbstractPolicy
from episodes import Environment
from pomdp import make_pomdp


# Torch leaks file descriptors when using queues. So raise file descriptor limit. https://github.com/pytorch/pytorch/issues/973#issuecomment-346405667
import resource
rlimit = resource.getrlimit(resource.RLIMIT_NOFILE)
resource.setrlimit(resource.RLIMIT_NOFILE, (8192, rlimit[1]))

@dataclass(order=True)
class EvaluatedPolicy:
	policy: AbstractPolicy = field(compare=False)
	return_given: float

def make_and_eval_offspring(
		parent: AbstractPolicy,
		env_str: str,
		return_estimator: Callable[[AbstractPolicy, Environment], float],
		count: int,
		std: float,
		n_procs) -> SortedList:
	"""Subroutine of openai_es_step: Make and evaluate count mutants from a single parent.
	Only keeps better mutants from opposite pairs.
	Returned list contains (count+1) // 2 EvaluatedPolicies, sorted ascendingly by return.
	"""
	def worker(
			mutant_q: JoinableQueue,
			count_left: SharedValueT,
			lock: LockT,
			parent: AbstractPolicy,
			env_str: str,
			return_estimator: Callable[[AbstractPolicy, Environment], float],
			std: float) -> None:
		"""Makes and evaluates mutants and puts them into mutant_q.
		Is run in parallel processes.
		"""
		env = make_pomdp(env_str)
		while True:
			with lock:
				if count_left.value == 0:
					break
				count_left.value -= 1
			m1, m2 = parent.make_opposite_mutants(std)
			r1 = return_estimator(m1, env)
			r2 = return_estimator(m2, env)
			mutant_q.put(
				EvaluatedPolicy(m1, r1) if r1 > r2 else EvaluatedPolicy(m2, r2))
			del m1, m2 # Helps with file descriptor leak mentioned above
		env.close()
		mutant_q.join() # Wait for parent process to take mutants out of queue. https://discuss.pytorch.org/t/tensors-as-items-in-multiprocessing-queue/411

	keptCount = (count+1) // 2
	mutantQ: JoinableQueue = JoinableQueue()
	countLeft = SharedValue("i", keptCount, lock=False)
	lock = Lock()
	set_num_threads(1) # See https://github.com/pytorch/pytorch/issues/17199#issuecomment-465313245. Apparently only necessary if the Policy has many parameters, like for BipedalWalker.
	procs = [Process(
		name=F"OE Breeder No. {i}",
		target=worker,
		args=(mutantQ, countLeft, lock, parent, env_str, return_estimator, std),
		daemon=True) for i in range(n_procs)]
	for p in procs:
		p.start()
	set_num_threads(cpu_count())
	mutants = SortedList()
	for _ in range(keptCount):
		mutants.add(mutantQ.get())
		mutantQ.task_done()
	for p in procs:
		p.join()
	return mutants

def openai_es_step(
		parent: AbstractPolicy,
		env_str: str,
		return_estimator: Callable[[AbstractPolicy, Environment], float],
		count: int,
		std: float,
		learning_rate: float,
		n_procs: int = 1) -> Tuple[AbstractPolicy, SortedList, float]:
	"""One generation with OpenAI-ES.
	Returns parent policy for next generation, SortedList of EvaluatedPolicies and mean return (given by return_estimator) of mutants.
	Learning rate is relative to mutation size (= std).
	"""
	evaluatedMutants = make_and_eval_offspring(
		parent, env_str, return_estimator, count, std, n_procs)
	rs = 0
	keptCount = len(evaluatedMutants) 
	assert(keptCount == (count+1) // 2)

	# Set child to average of mutants weighed by their index
	childParams = zeros_like(parent.parameter_vector())
	for weight, evaluatedMutant in enumerate(evaluatedMutants, start=1):
		childParams.add_(evaluatedMutant.policy.parameter_vector(), alpha=weight)
		rs += evaluatedMutant.return_given # to calculate mean return
	childParams.mul_(learning_rate * 2 / (keptCount * (keptCount+1))) # divide by sum of weights

	childParams.add_(parent.parameter_vector(), alpha=1-learning_rate) # child is parent + lr*(AVG(mutants)-parent)
	return parent.new_from_params(childParams), evaluatedMutants, rs/keptCount

def genetic_algorithm_step(
		parents: List[EvaluatedPolicy],
		env_str: str,
		return_estimator: Callable[[AbstractPolicy, Environment], float],
		mutants_count: int,
		std: float,
		n_procs: int = 1) -> List[EvaluatedPolicy]:
	"""One generation with the genetic algorithm.
	Makes mutants and returns best of them as parents for next generation.
	Unmutated parents also compete, reevaluated, so the number of policies evaluated is mutants_count + len(parents).
	"""
	def worker(
			policies_q: SimpleQueue,
			evaled_policies_q: JoinableQueue,
			env_str: str,
			return_estimator: Callable[[AbstractPolicy, Environment], float],
			std: float) -> None:
		"""Evaluates policies from policies_q and puts them into evaled_policies_q.
		Is run in parallel processes.
		"""
		env = make_pomdp(env_str)
		while True:
			m = policies_q.get()
			if m is None:
				break
			evaled_policies_q.put(EvaluatedPolicy(m, return_estimator(m, env)))
		env.close()
		evaled_policies_q.join() # See comment at corresponding line in the OpenAI-ES worker

	def policies_generator_function() -> Iterator[AbstractPolicy]:
		"""Yields a total of mutants_count + len(parents) policies.
		"""
		for _ in range(mutants_count):
			yield random_choice(parents).policy.make_mutant(std)
		yield from (ea.policy for ea in parents) # Possible BUG? Swapping this line with the for loop results in the parent process not receiving any EvaluatedPolicies, even though it shouldn't make a difference.

	def evaled_policies_memorizer_function() \
			-> Generator[List[EvaluatedPolicy], EvaluatedPolicy, None]:
		bestEvPos: List[EvaluatedPolicy] = []
		for _ in range(len(parents)): # Memorize first len(parents) policies
			heappush(bestEvPos, (yield bestEvPos))
		while True: # Keep memorizing only the best len(parents) policies
			heappushpop(bestEvPos, (yield bestEvPos))

	policiesQ: SimpleQueue = SimpleQueue()
	evaledPoliciesQ: JoinableQueue = JoinableQueue()
	policiesGenerator = policies_generator_function()
	for _ in range(min(2*n_procs, mutants_count+len(parents))): # 2: Try to lower the chance that policiesQ becomes empty
		policiesQ.put(next(policiesGenerator))
	set_num_threads(1) # See comment at identical line in OpenAI-ES method.
	procs = [Process(
		name=F"GA Breeder No. {i}",
		target=worker,
		args=(policiesQ, evaledPoliciesQ, env_str, return_estimator, std),
		daemon=True) for i in range(n_procs)]
	for p in procs:
		p.start()
	set_num_threads(cpu_count())
	evaledPoliciesMemorizer = evaled_policies_memorizer_function()
	next(evaledPoliciesMemorizer) # Initialization
	for _ in range(mutants_count+len(parents)):
		evaledPolicies = evaledPoliciesMemorizer.send(evaledPoliciesQ.get())
		evaledPoliciesQ.task_done()
		try:
			policiesQ.put(next(policiesGenerator))
		except StopIteration:
			policiesQ.put(None) # Signal workers to stop
	for p in procs:
		p.join()
	return evaledPolicies
