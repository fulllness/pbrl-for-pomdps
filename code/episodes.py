# SPDX-FileCopyrightText: 2023 Lukas Fülle <https://codeberg.org/fulllness>
# SPDX-License-Identifier: MPL-2.0


from math import inf as infinity, sqrt
from random import normalvariate, random, randrange
from typing import Any, Callable, List, Optional, Tuple, TypeVar, Union, cast

from numpy import array, float32, ndarray
from torch import Tensor, cat as cat_tensors, from_numpy, stack as tensor_stack, tensor, unsqueeze

from abstractpolicy import AbstractPolicy
from pomdp import Environment


A = TypeVar("A")
Recording = Tensor # Sequence of observations and actions from one episode
Reward = Union[float, Tensor]
Action = Union[ndarray, int]


def episode(
		po: AbstractPolicy,
		env: Environment,
		aggregator: Callable[[A, Tensor, Reward, Action], A],
		initial_aggregate: A) -> A:
	"""Let agent act in environment using policy po.
	Does a fold with the aggregator function, feeding it the observation, action and reward of each step.
	"""
	po.reset() # type: ignore # Suppress erroneous mypy error: "Tensor" not callable
	observation_np = env.reset()
	aggregate = initial_aggregate
	reward = 0.0
	action: Action = array([0.0], dtype=float32)
	done = False
	while True:
		observation = from_numpy(observation_np)
		aggregate = aggregator(aggregate, observation, reward, action)
		if done:
			break
		action = po.decide(observation)
		observation_np, reward, done = env.step(action)
	return aggregate

def true_return(po: AbstractPolicy, env: Environment) -> float:
	"""Get true return of one episode of agent acting in environment using policy po.
	"""
	return episode(
		po,
		env,
		(lambda cum_rew, obs, rew, act: cum_rew + rew),
		tensor(0.0)).item()


def observations_actions_and_return(
		po: AbstractPolicy,
		env: Environment,
		max_len: Optional[int] = None) -> Tuple[Recording, float]:
	"""Get observations and actions (= Recording) and true return of one episode of agent acting in environment using policy po.
	Returned Tensor contains observations and actions, concatenation of observation components and action indexed through the first dimension and time step through the second.
	Set max_len to limit the Recording and the respective return to a random subinterval of the episode. 
	"""
	def aggor(
			cum_obs_rew_n_act: Tuple[List[Tensor], List[Reward], List[Action]],
			obs: Tensor,
			rew: Reward,
			act: Action) -> Tuple[List[Tensor], List[Reward], List[Action]]:
		cum_obs_rew_n_act[0].append(obs)
		cum_obs_rew_n_act[1].append(rew)
		cum_obs_rew_n_act[2].append(act)
		return cum_obs_rew_n_act

	initOL: List[Tensor] = []
	initRL: List[Reward] = []
	initAL: List[Action] = []
	obsList, rewList, actList = episode(po, env, aggor, (initOL, initRL, initAL))
	if max_len is not None and max_len < len(obsList):
		startIdx = randrange(len(obsList) - max_len + 1)
		obsList = obsList[startIdx : startIdx+max_len]
		rewList = rewList[startIdx : startIdx+max_len]
		actList = actList[startIdx : startIdx+max_len]
	ret: Reward = 0.0
	for rew in rewList:
		ret += rew
	return (
		cat_tensors((tensor_stack(obsList, 1), tensor(array(actList)).T), 0),
		cast(float, cast(Tensor, ret).item() if type(ret) is Tensor else ret)
		)


def corrupted_return(p: float) -> Callable[[AbstractPolicy, Environment], float]:
	"""Transformation of returns to decrease information content similar to corrupted preferences (see returnlearning.py) by setting a fraction of returns to +/- infinity.
	"""
	c = 1 - sqrt(1 - 2*p) # This fraction of returns is corrupted to lead to a fraction of p corrupt (false) preferences, counting neutral preferences as half corrupt.
	def f(po: AbstractPolicy, env: Environment):
		r = random()
		return true_return(po, env) if r>c else -infinity if r<c/2 else infinity
	return f

def noisy_return(std: float) -> Callable[[AbstractPolicy, Environment], float]:
	"""Get return of one episode of agent acting in environment plus normal noise.
	Curried for convenience.
	"""
	return lambda po, env: normalvariate(true_return(po, env), std) # normalvariate instead of gauss for thread-safety
