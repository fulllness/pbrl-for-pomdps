# SPDX-FileCopyrightText: 2023 Lukas Fülle <https://codeberg.org/fulllness>
# SPDX-License-Identifier: MPL-2.0

# SPDX-FileCopyrightText: 2018 CMU Locus Lab <https://github.com/locuslab/TCN>
# SPDX-License-Identifier: MIT


from typing import List

from torch import Tensor, nn, no_grad, squeeze, sum as tensor_sum, unsqueeze

from abstractpolicy import AbstractPolicy
from episodes import Environment, Recording, observations_actions_and_return
from tcn import TemporalConvNet


class ReturnModel(nn.Module):
	def __init__(
			self,
			input_size: int,
			num_channels: List[int],
			kernel_size: int,
			dropout: float) -> None:
		super(ReturnModel, self).__init__()
		self.tcn = TemporalConvNet(
			input_size, num_channels, kernel_size=kernel_size, dropout=dropout)
		self.linear = nn.Linear(num_channels[-1], 1)
		self.init_weights()

	@staticmethod
	def for_env(env_str: str, kernel_size: int, neurons_per_layer: int) \
			-> "ReturnModel":
		if env_str == "CopyMem":
			return ReturnModel(1+1, [neurons_per_layer]*2, kernel_size, 0) # receptive field for kernel_size=5: (5-1) * (2^2-1) + 1 = 13
		elif env_str == "CartPole-v1":
			return ReturnModel(4+1, [neurons_per_layer]*1, kernel_size, 0) # (5-1) * (2^1-1) + 1 = 5
		elif env_str == "Pendulum-v1":
			return ReturnModel(3+1, [neurons_per_layer]*3, kernel_size, 0) # (5-1) * (2^3-1) + 1 = 29
		elif env_str == "Pendulum-v1-only_v":
			return ReturnModel(1+1, [neurons_per_layer]*3, kernel_size, 0) # (5-1) * (2^3-1) + 1 = 29
		elif env_str == "Pendulum-v1-only_xy":
			return ReturnModel(2+1, [neurons_per_layer]*3, kernel_size, 0) # (5-1) * (2^3-1) + 1 = 29
		elif env_str == "LunarLander-v2":
			return ReturnModel(8+1, [neurons_per_layer]*6, kernel_size, 0) # (5-1) * (2^6-1) + 1 = 253
		elif env_str == "LunarLander-v2-only_xya":
			return ReturnModel(3+1, [neurons_per_layer]*6, kernel_size, 0) # (5-1) * (2^6-1) + 1 = 253
		else:
			raise ValueError(F"Unknown environment for ReturnModel: {env_str}")

	def init_weights(self) -> None:
		self.linear.weight.data.normal_(0, 0.01)

	def forward(self, x: Tensor) -> Tensor:
		"""To be called with a batch of any number of observation Recordings stacked in one Tensor.
		"""
		return squeeze(self.linear(tensor_sum(self.tcn(x), 2)), 1)

	def predict_rec(self, rec: Recording) -> float:
		self.eval()
		with no_grad():
			return self(unsqueeze(rec, 0)).item()

	def predict_po_env(self, po: AbstractPolicy, env: Environment) -> float:
		return self.predict_rec(observations_actions_and_return(po, env)[0])
