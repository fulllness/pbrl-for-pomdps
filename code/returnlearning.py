# SPDX-FileCopyrightText: 2023 Lukas Fülle <https://codeberg.org/fulllness>
# SPDX-License-Identifier: MPL-2.0


from math import inf as infinity, sqrt
from random import gauss, random, randrange
from typing import Callable, Iterator, List, Optional, Tuple

from torch import Tensor, no_grad, stack as tensor_stack, tensor
from torch.nn.functional import binary_cross_entropy_with_logits, pad as tensor_pad
from torch.optim import Optimizer

from abstractpolicy import AbstractPolicy
from episodes import Environment, Recording, observations_actions_and_return
from returnmodel import ReturnModel


class PreferenceBuffer:
	"""Stores triples of two recordings and a probability of the first being preferred over the second.
	Drops old ones once size is reached.
	Implemented as a ring buffer.
	"""

	def __init__(self, size: int) -> None:
		self.max_size = size
		self.used_size = 0
		self.next_insert = 0
		self.firsts: List[Tensor] = [tensor(0)] * size
		self.seconds: List[Tensor] = [tensor(0)] * size
		self.p_first_betters: List[Tensor] = [tensor(0)] * size

	def insert(self, first: Recording, second: Recording, p_first_better: Tensor) \
			-> None:
		"""p_first_better is a zero-dimensional Tensor.
		"""
		self.firsts[self.next_insert] = first
		self.seconds[self.next_insert] = second
		self.p_first_betters[self.next_insert] = p_first_better
		if self.used_size != self.max_size:
			self.used_size += 1
		self.next_insert = (self.next_insert+1) % self.max_size

	def batch(self, number: int) -> Tuple[Tensor, Tensor, Tensor]:
		"""Returns given number of random triples, aggregated by components.
		Observation recordings are padded left with zeros to be of equal length.
		"""
		def substack(lyst: List[Recording]) -> Tensor:
			max_len = max(lyst[i].size(1) for i in idc)
			return tensor_stack(
				[tensor_pad(lyst[i], (max_len-lyst[i].size(1), 0)) for i in idc])

		idc = [randrange(self.used_size) for _ in range(number)]
		return (
			substack(self.firsts),
			substack(self.seconds),
			tensor_stack([self.p_first_betters[i] for i in idc]))

def sign(x: float) -> int:
	#return (x>0) - (x<0) # Doesn't work when x is numpy array
	return -1 if x<0 else 0 if x==0 else 1

def rational_pref(r1: float, r2: float, resolution: float = 0) -> float:
	"""Perfectly accurate preferences.
	Neutral preference if difference of returns is at most resolution.
	"""
	return 0.5 if abs(r1-r2) <= resolution else (sign(r1-r2) + 1) / 2

def corrupted_pref(p: float) -> Callable[[float, float], float]:
	"""Rational preference, flipped with probability p.
	"""
	return (lambda r1, r2:
		1 - rational_pref(r1, r2) if random()<p else rational_pref(r1, r2))

def noisy_pref(std: float) -> Callable[[float, float], float]:
	"""Rational preference after Gaußian noise is added to both returns.
	Resolution is set so that equal true returns have a ~95% chance of inducing a neutral preference.
	"""
	# Only the difference between the returns matters, we so actually add double the noise to one return.
	double_std = std * sqrt(2) # The sum of Gaußian variables has variance equal to the sum of the single variances. Variance is the square of standard deviation.
	return lambda r1, r2: rational_pref(r1, gauss(r2, double_std), 2*double_std)

def do_logits_match_preference(logits: float, pref: float) -> bool:
	return pref==0.5 or (pref-0.5) * logits > 0 # If the preference is neutral, always count as matching

def test_return_model_with_preference(
		rm: ReturnModel,
		po1: AbstractPolicy,
		po2: AbstractPolicy,
		env: Environment,
		max_rec_len: Optional[int],
		preferrer: Callable[[float, float], float],
		pb: PreferenceBuffer,
		device: str) -> Tuple[bool, bool, float]:
	"""Store observation Recordings (optionally of a maximum length) from two Policies and a preference expressed by preferrer based on their true returns in the given PreferenceBuffer, and return whether the modelled returns match the preference, whether they match the true returns, and the logits predicting the preference.
	"""
	o1, r1 = observations_actions_and_return(po1, env, max_rec_len)
	o2, r2 = observations_actions_and_return(po2, env, max_rec_len)
	o1 = o1.to(device)
	o2 = o2.to(device)
	logits = rm.predict_rec(o1) - rm.predict_rec(o2)
	p = preferrer(r1, r2)
	pb.insert(o1, o2, tensor(p).to(device))
	return (
		do_logits_match_preference(logits, p),
		do_logits_match_preference(logits, rational_pref(r1, r2)),
		logits if p==1 else -logits if p==0 else abs(logits))

def train(rm: ReturnModel, pb: PreferenceBuffer, bs: int, op: Optimizer) -> float:
	"""Train rm with a batch of size bs from pb.
	Returns loss.
	"""
	rm.train()
	op.zero_grad()
	fst, snd, pfb = pb.batch(bs)
	loss = binary_cross_entropy_with_logits(rm(fst)-rm(snd), pfb)
	loss.backward()
	op.step()
	return loss.item()

class MovingAverage:
	"""Exponentially weighted moving average.
	"""

	def __init__(self, smoothing: float, start: float) -> None:
		self.smoothing = smoothing
		self.average = start

	def smooth(self, value: float) -> float:
		self.average = self.smoothing * (self.average - value) + value
		# Equivalent: self.average = smoothing * self.average + (1-smoothing) * value
		return self.average

	def get(self) -> float:
		return self.average

class ConvergenceDetector:
	"""Used to detect when a sequence of values approximately converges.
	Signals convergence when the number of consecutive new terms not setting a new minimum or maximum reaches a patience threshold.
	New maximum or minimum is only set if old one is surpassed by more than d_min.
	"""

	def __init__(self, patience: int, d_min: float = 0) -> None:
		self.patience = patience
		self.d_min = d_min
		self.reset()

	def reset(self) -> None:
		self.minimum = infinity
		self.maximum = -infinity

	def detect(self, value: float) -> bool:
		newMimum = False
		if value < self.minimum - self.d_min:
			self.minimum = value
			newMimum = True
		if value > self.maximum + self.d_min: # Only on the first call after a reset are both if-clauses executed
			self.maximum = value
			newMimum = True
		if newMimum:
			self.remaining_patience = self.patience
		else:
			self.remaining_patience -= 1
		return self.remaining_patience <= 0

def test_and_train(
		rm: ReturnModel,
		pos: Iterator[AbstractPolicy],
		env: Environment,
		max_rec_len: Optional[int],
		preferrer: Callable[[float, float], float],
		pb: PreferenceBuffer,
		device: str,
		bs: int,
		op: Optimizer,
		loss_cd: ConvergenceDetector,
		current_acc: MovingAverage,
		acc_cd: ConvergenceDetector,
		true_acc: MovingAverage) -> Tuple[int, int, float, float]:
	"""Repeatedly test ReturnModel on new preferences and train it, until the moving average of its accuracy converges.
	The training after each test is done until the loss converges.
	"""
	newPrefs = 0
	trainSteps = 0
	firstLogits = None
	firstLoss = None
	acc_cd.reset()
	while True:
		newPrefs += 1
		match_pref, match_true, logits = test_return_model_with_preference(
			rm, next(pos), next(pos), env, max_rec_len, preferrer, pb, device)
		firstLogits = firstLogits or logits
		loss_cd.reset()
		while True:
			trainSteps += 1
			loss = train(rm, pb, bs, op)
			firstLoss = firstLoss or loss
			if loss_cd.detect(loss):
				break
		true_acc.smooth(float(match_true))
		if acc_cd.detect(current_acc.smooth(float(match_pref))):
			break
	return newPrefs, trainSteps, firstLogits, firstLoss # current_acc and true_acc are passed by reference, so no need to return them
