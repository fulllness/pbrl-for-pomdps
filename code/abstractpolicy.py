# SPDX-FileCopyrightText: 2023 Lukas Fülle <https://codeberg.org/fulllness>
# SPDX-License-Identifier: MPL-2.0


from random import choices
from typing import  Callable, Iterator, Sequence, Tuple, Union, cast

from numpy import ndarray
from torch import Tensor, argmax, exp as tensor_exp, nn, randn_like


class AbstractPolicy(nn.Module):

	def __init__(self, env_str: str) -> None:
		super(AbstractPolicy, self).__init__()
		self.env = env_str
		self.decide: Union[Callable[[Tensor], ndarray], Callable[[Tensor], int]]

	def new_from_params(self, params: Tensor) -> "AbstractPolicy":
		"""Make new policy with given parameter vector.
		Type and environment are inherited from self.
		"""
		p = type(self)(self.env)
		nn.utils.vector_to_parameters(params, p.parameters())
		return p

	def parameter_vector(self) -> Tensor:
		return nn.utils.parameters_to_vector(self.parameters())


	# Different functions for determining actions, one of which is assigned to self.decide

	def raw(self, obs: Tensor) -> ndarray:
		return self(obs).numpy()

	def binary(self, obs: Tensor) -> int:
		return 0 if self(obs)<0 else 1

	def n_ary_det(self, obs: Tensor) -> int:
		return cast(int, argmax(self(obs)).item())

	def n_ary_sto(self, obs: Tensor) -> int:
		out = self(obs)
		return choices(range(len(out)), cast(Sequence[float], tensor_exp(out)))[0] # Single weighted choice


	def make_noise(self, std) -> Tensor:
		"""Make vector with the shape of self's parameters, filled with samples from N(0,std^2)
		"""
		x = randn_like(self.parameter_vector())
		x.mul_(std)
		return x

	def make_mutant(self, std: float) -> "AbstractPolicy":
		"""Make new Policy with parameters of self plus noise from N(0,std^2).
		"""
		x = self.make_noise(std)
		x.add_(self.parameter_vector())
		return self.new_from_params(x)

	def make_opposite_mutants(self, std: float) \
			-> Tuple["AbstractPolicy", "AbstractPolicy"]:
		"""Make two new Policies with parameters of self +/- x with x from N(0,std^2).
		"""
		x = self.make_noise(std)
		m1 = self.new_from_params(self.parameter_vector() - x)
		x.add_(self.parameter_vector())
		return m1, self.new_from_params(x)

	def mutant_generator(self, std: float) -> Iterator["AbstractPolicy"]:
		while True:
			yield self.make_mutant(std)
