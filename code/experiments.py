# SPDX-FileCopyrightText: 2023 Lukas Fülle <https://codeberg.org/fulllness>
# SPDX-License-Identifier: MPL-2.0


# All experiments for the paper. Restarting the Python instance for each line is advised.

# Return learning
# pref_ga("history", "come", 7)
# pref_ga("history", "pendxy", 7)

# Reward learning
# pref_ga("history", "come", 1)
# pref_ga("history", "pendxy", 1)

# True return
# only_ga("history", "come")
# only_ga("history", "pendxy")

# Policy without memory
# only_ga("amnesisa", "come")
# only_ga("amnesisa", "pendxy")


from os import cpu_count

from concretepolicies import AmnesiaPolicy, HistoryPolicy
from doruns import setup_ga, setup_rm, train_ga


LOGDIR = "../data/"
REPEATS = 8 # per experimental condition
NUMBER_OF_PROCESSES = cpu_count()

ENV_PARAMS = { # (env_str, number of episodes, ga_std, neurons per layer in return model depending on kernel size)
	"come": ("CopyMem", 30000, 0.03, {1:10, 7:4}), # RM params: 441, 441
	"pendxy": ("Pendulum-v1-only_xy", 20000, 0.1, {1:32, 7:12}) # 5761, 5497
	}
POLICIES = {
	"amnesia": AmnesiaPolicy,
	"history": HistoryPolicy
	}
GA_PARENT_COUNT = 16
GA_CHILD_COUNT = 16
LOSS_PATIENCE = 1
ACCURACY_PATIENCE = 1

def repeat(experiment):
	for n in range(1, REPEATS+1):
		experiment(n)

def su_ga(policy_class, env_str, std, log):
	setup_ga(
		policy_class,
		env_str,
		GA_PARENT_COUNT,
		GA_CHILD_COUNT,
		std,
		NUMBER_OF_PROCESSES,
		csv_filename=LOGDIR+log,
		log_tensorboard_=True,
		logc=F"exprm_{log}")


def only_ga(policy_class, env):
	env_str, eps, std, _ = ENV_PARAMS[env]
	def f(n):
		su_ga(
			POLICIES[policy_class],
			env_str,
			std,
			F"only_ga_{env}_{policy_class}_{n}")
		train_ga(eps)
	repeat(f)

def pref_ga(policy_class, env, kernel_size):
	env_str, eps, std, rm_widths = ENV_PARAMS[env]
	def f(n):
		fn = F"pref_ga_{env}_{policy_class}_k{kernel_size}_{n}"
		setup_rm(
			env_str,
			ACCURACY_PATIENCE,
			LOSS_PATIENCE,
			kernel_size,
			rm_widths[kernel_size],
			rm_csv_filename=LOGDIR+fn+"_rm")
		su_ga(POLICIES[policy_class], env_str, std, fn)
		train_ga(eps)
	repeat(f)
