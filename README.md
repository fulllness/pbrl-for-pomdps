<!--
SPDX-FileCopyrightText: 2023 Lukas Fülle <https://codeberg.org/fulllness>
SPDX-License-Identifier: MPL-2.0
-->

# Evolutionary Preference-Based Reinforcement Learning for Partially Observable Environments

Code and data for the [paper of the above title](https://openreview.net/forum?id=dOUe9M67by).
Quoting its abstract:
 > A significant challenge in reinforcement learning is how to accurately convey our desires to the artificial agent. Preference-based reinforcement learning uses human preferences between concrete examples of the agent’s behavior to model the reward or the return function the human intends. However, the existing models discard much information and can therefore be less accurate than possible, especially if the environment is only partially observable.
 >
 > To overcome this limitation, the model presented in this work combines all available information (all observations made during one episode) through a temporal convolutional net to model the return function, instead of the reward function, from preferences. The reinforcement learning – implemented with a genetic algorithm – is then guided by this model.
 >
 > We show that our method is a viable way to apply preference-based reinforcement learning in partially observable environments.


## Usage

Use Python to execute the code.


### Experiments

Each experimental condition is tested by one function call:
```python
import experiments
experiments.pref_ga("history", "pendxy", 7)
```
In this example, preferences are used to train a policy with memory (`"history"`) on the Pendulum-xy task with the return model having a kernel size of 7.
Look into experiments.py for further information.

The results are logged into csv files in `../data/` whose names reflect the arguments of the above function call.
Additionally, TensorBoard logs are created in `runs/`.

For the Pendulum-xy task, sample policies performing in the environment are rendered every so often.


### Testing

The underlying functions from doruns.py can also be called directly.
This is useful for testing, as it allows for more interactivity in the REPL: pausing (Ctrl+C), inspecting the state (see global variables in doruns.py) and continuing (calling `train_ga` again).

```python
from os import cpu_count

from concretepolicies import AmnesiaPolicy, HistoryPolicy
from doruns import *

setup_rm(
	"Pendulum-v1-only_xy",
	acc_patience=1,
	loss_patience=1,
	kernel_size_=7,
	neurons_per_layer=12
)
setup_ga(
	HistoryPolicy,
	"Pendulum-v1-only_xy",
	p_count=16,
	m_count_=16,
	std_=0.1,
	n_p_=cpu_count(),
	logc="comment_for_log_filename"
)
train_ga(episodes=20000)
```

Doing so only logs via TensorBoard per default, but this can be changed through optional arguments – see source code.


## Where to find hyperparameters

+ experiments.py:
	+ Parameters of genetic algorithm
	+ Kernel size & width of return models
	+ Patience for loss convergence detector
+ concretepolicies.py:
	+ Architectures of policies
+ returnmodel.py:
	+ Input size & depth of return models
+ doruns.py:
	+ Size of preference buffer
	+ Learning rate for return model
	+ Smoothing factors for accuracy
	+ Minimum change for convergence detectors
	+ Batch size for return model training
